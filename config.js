const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync') 

const adapter = new FileSync('config.json')
const db = low(adapter)

db.defaults({
    config: {
        consecutivo: 0,
        firebird: {
            host: '127.0.0.1',
            port: 3050,
            database_casa: 'C:\\casawin\\csaaiwin\\datos\\casa.gdb',
            database_cgestor: 'C:\\casawin\\csaaiwin\\datos\\cgestor.gdb',
            user: '',
            password: '',
            lowercase_keys: false,
            role: null,
            pageSize: 4096
        },
        classifile: {
            host: '',
            user: '',
            password: ''
        },
        clasificar: {
            category: '',
            keywords: {}
        }
    } 
}).write()

module.exports = db
