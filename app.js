// Modulos Node
const fs = require('fs')
const path = require('path')
const Firebird = require('node-firebird')
const request = require('request')

// Bases de datos locales
const config = require('./config') // Configuracion de la app
const nouploads = require('./nouploads') // Gestion de archivos no cargados
const noexists = require('./noexists') // Gestion de archivos que no existen

// Inicialización de la app
const app = new Vue({
    el: '#app',

    data: {
        // Indicador para saber si la aplicacion esta corriendo
        running: false,

        // Lista de archivos que no se han cargado
        nuList: [],

        // Editor de la configuración de la app
        editor: null,

        // Configuración del sistema
        // Almacena la configuración de la db config
        config: {},

        // Registros de los archivos
        rows: [],

        // Datos de los archivos
        data: [],

        // Tiempo de espera para
        // ejecutar un proceso
        timeout: null
    },

    // Al iniciar la aplicación
    created() {
        // Obtener configuración del archivo de configuración
        this.config = config.get('config').value()
    },

    // Al terminar de cargar la aplicación
    mounted() {
        // Establecer elemento del editor
        this.editor = new JSONEditor(this.$refs.jsoneditor)

        // Establecer configuración en el
        // editor de configuración
        this.editor.set(this.config)
    },

    computed: {
        // Convertir las referencias obtenidas
        // en un arreglo para firebird
        refs() {
            // Almacenar los numeros de referencia
            var refs = []

            // Obtener los numeros de referencia
            for (let row of this.rows) {
                let ref = '' + row.NUM_REFE
                ref = `'${ref}'`

                if (!refs.includes(ref)) {
                    refs.push(ref)
                }
            }

            return '(' + refs.join(',') + ')'
        },

        // Query para la busqueda de documentos
        // por numero consecutivo
        consquery() {
            let query = ''

            // Verificar si existen documentos no cargados
            for (let nuItem of this.nuList) {
                if (nuItem !== undefined) {
                    query = `SELECT * FROM CGESTO_ARCHDIG WHERE NUM_CONS = ${nuItem.NUM_CONS}`
                    break
                }
            }

            return query || `SELECT FIRST 100 * FROM CGESTO_ARCHDIG
                WHERE NUM_CONS > ${this.config.consecutivo} ORDER BY NUM_CONS ASC`
        }
    },

    methods: {
        // Actualizar configuración
        save() {
            // Verificar que la aplicación no este corriendo
            if (this.running === true) {
                let msg = 'Por favor detenga el proceso para aplicar los cambios...'

                console.warn(msg);
                alert(msg)

                return
            }

            // Actualizar propiedad con los cambios
            this.config = this.editor.get()

            // Actualizar el archivo de configuración
            config.set('config', this.config).write()

            let msg = 'Los cambios se establecieron correctamente...'

            console.info(msg);
            alert(msg)
        },

        // Desactivar el proceso de carga
        stop() {
            // Desactivar proceso
            this.running = false

            // Limpiar procesos en espera
            clearTimeout(this.timeout)

            // Mostrar mensaje
            console.info('Proceso detenido...')
        },

        // Activar proceso de lectura de documentos
        run() {
            // Verificar si ya hay un proceso corriendo
            if (this.running === true) {
                return
            }

            // Activar proceso
            this.running = true

            // Iniciar proceso
            setTimeout(() => {
                this.process()
            }, 1000)

            console.log('Proceso iniciado...')
        },

        // Iniciar proceso
        process() {
            // Limpiar registros y datos
            // de los documentos
            this.rows = []
            this.data = []

            // Siempre se verifica
            // que la app siga corriendo despues de cada tarea
            if (this.running === false) {
                return
            }

            // Buscar archivos para subir
            this.search4Files(() => {
                // Verificar que siga corriendo
                if (this.running === false) {
                    return
                }

                // Esperar un poco antes de buscar 
                // la información de los documentos
                setTimeout(() => {
                    // Buscar información de los archivos obtenidos
                    this.search4Data(() => {
                        // Leer registros para subirlos
                        this.reader()
                    })
                }, 200)
            })
        },

        // Buscar archivos
        search4Files(callback) {
            this.attach('cgestor', this.consquery, rows => {
                this.rows = rows || []

                // Si no se encontraron documentos y si se trata
                // de un archivo no cargado
                if (this.rows.length < 1 && (this.nuList.length > 0 && this.nuList[0] !== undefined)) {
                    // Quitar este archivo de la lista de no cargados
                    nouploads.get('nouploads')
                        .remove({ NUM_CONS: this.nuList[0].NUM_CONS }).write()
                }

                callback()
            })
        },

        // Buscar datos de los registros obtenidos
        search4Data(callback) {
            const query = `SELECT pedime.*, client.*
                FROM SAAIO_PEDIME pedime
                LEFT JOIN CTRAC_CLIENT client
                ON pedime.CVE_IMPO = client.CVE_IMP
                WHERE pedime.NUM_REFE IN ${this.refs}`

            // Verificar que existan registros
            // para obtener información
            if (this.rows.length > 0) {
                this.attach('casa', query, data => {
                    this.data = data || []

                    callback()
                })
            } else {
                callback()
            }
        },

        // Obtener una base de datos
        attach(db, query, callback) {
            // Establecer base de datos
            this.config.firebird.database = this.config.firebird['database_' + db]

            // Adjuntar la configuración de la base de datos
            Firebird.attach(this.config.firebird, (err, db) => {
                const fn = e => {
                    // Liberar base de datos
                    if (db !== undefined) {
                        try {
                            db.detach()
                        } catch (ex) {
                            console.warn(ex)
                        }
                    }

                    console.warn(e)
                    console.log('Ocurrio un problema')
                    console.log('Iniciando un nuevo proceso')

                    this.timeout = setTimeout(() => {
                        this.process()
                    }, 16000)
                }

                if (err) {
                    fn(err); return
                }

                // Ejecutar query
                db.query(query, (_err, data) => {
                    if (_err) {
                        fn(_err)
                    } else {
                        try {
                            db.detach()
                        } catch (ex) {
                            console.warn(ex)
                        }

                        callback(data)
                    }
                })
            })
        },

        // Comenzar lectura de registros
        reader() {
            // Verificar si el proceso debe detenerse
            if (this.running === false || navigator.onLine === false) {
                return
            } else if (this.rows.length < 1) {
                this.processComplete()
                return
            }

            // Obtener un registro para procesar
            const row = this.rows.shift()

            // Preparar ruta del archivo
            row.FILE_PATH = path.join('' + row.RUT_ARCH, '' + row.NOM_ARCH)

            // Obtener llaves para la clasificación
            const keywords = this.getKeywords(row)

            var readStream = null

            try {
                if (!fs.existsSync(row.FILE_PATH)) {
                    row.ERROR = "No existe"

                    // console.warn(row.FILE_PATH, row.ERROR)

                    this.noexists(row)

                    this.reader()

                    return
                } else if (Object.keys(keywords).length < 1) {
                    row.ERROR = "No se geraron llaves"

                    //console.warn(row.FILE_PATH, row.ERROR)

                    this.noupload(row)

                    this.reader()

                    return
                }

                readStream = fs.createReadStream(row.FILE_PATH)
            } catch (err) {
                row.ERROR = err

                console.error(err)

                this.noupload(row)

                // Esperar un poco para volver a intentar
                this.timeout = setTimeout(() => {
                    this.reader()
                }, 1200)

                return
            }

            request.post({
                url: this.makeUrl(keywords),
                formData: {
                    file: readStream
                }
            }, (err, res) => {
                // Acciones a completar despues de
                // enviar el archivo
                if (res === undefined) {
                    res = {
                        body: err
                    }
                }
                
                this.uploadComplete(row, res.body)

                if (res.body === "success") {
                    console.info(row.FILE_PATH, 'success')
                } else {
                    row.ERROR = err

                    // console.warn(row.FILE_PATH, row.ERROR)

                    this.noupload(row)
                }

                this.reader()
            })
        },

        // Se ejecuta cuando el metodo -reader- termina
        // de ejecutarse
        processComplete() {
            // Obtener registros que no se han cargado
            if (this.nuList.length > 0) {
                this.nuList = []
            } else {
                this.nuList = nouploads.get('nouploads').value() || []

                if (this.nuList.length > 0) {
                    this.nuList = this.nuList.filter(o => {
                        switch (o.ERROR) {
                            case 'No existe':
                                return false
                            default:
                                return true
                        }
                    })
                }

                this.nuList = [this.nuList[0]]
            }

            // Iniciar proceso de carga de documentos
            this.timeout = setTimeout(() => {
                this.process()
            }, 1200)
        },

        // Guardar un archivo que no se cargo
        // para procesar mas tarde
        noupload(row) {
            // Buscar en la lista de no cargados
            const found = nouploads.get('nouploads')
                .find({
                    NUM_CONS: row.NUM_CONS
                }).value()

            if (found === undefined) {
                nouploads.get('nouploads').push({
                    NUM_REFE: row.NUM_REFE,
                    NUM_CONS: row.NUM_CONS,
                    RUT_ARCH: row.FILE_PATH,
                    ERROR: row.ERROR
                }).write()
            }
        },

        // Registrar archivos que no existen
        // en el directorio espicificado
        noexists(row) {
            // Buscar en la lista de no cargados
            const found = nouploads.get('nouploads')
                .find({ NUM_CONS: row.NUM_CONS }).value()

            if (found !== undefined) {
                // Quitar este archivo de la lista de no cargados
                nouploads.get('nouploads')
                    .remove({ NUM_CONS: row.NUM_CONS }).write()
            }

            noexists.get('noexists').push({
                NUM_REFE: row.NUM_REFE,
                NUM_CONS: row.NUM_CONS,
                RUT_ARCH: row.FILE_PATH
            }).write()
        },

        // Crear URL para la carga
        // del documento
        makeUrl(keywords) {
            return 'http://' + this.config.classifile.host + '/es/ws/uploader?' +
                'usr=' + this.config.classifile.user + '&' +
                'pwd=' + this.config.classifile.password + '&' +
                'ctg=' + this.config.clasificar.category + '&' +
                'keys=' + JSON.stringify(keywords)
        },

        // Obtener llaves
        getKeywords(row) {
            const keywords = {}

            // Obtener los datos relacionados con el documento
            var data = this.data.find(d => {
                return '' + d.NUM_REFE === '' + row.NUM_REFE
            }) || {}

            // Fucionar los datos
            data = Object.assign(data, row)

            for (let key of Object.keys(this.config.clasificar.keywords)) {
                let cfg_key = this.config.clasificar.keywords[key]

                if (data[cfg_key]) {
                    keywords[key] = '' + data[cfg_key]
                }
            }

            return keywords
        },

        // Acciones a realizar al completar una carga
        // de un documentos
        uploadComplete(row, res) {
            // Verificar si esta corriendo el 
            // proceso de archivos no cargados
            if (this.nuList.length > 0) {
                if (res === 'success') {
                    // Eliminar archivo de la lista de pendientes
                    nouploads.get('nouploads')
                        .remove({
                            NUM_CONS: row.NUM_CONS
                        }).write()
                }
            } else {
                // Establecer el siguiente numero consecutivo

                this.config.consecutivo = row.NUM_CONS

                this.editor.set(this.config)

                config.set('config.consecutivo', row.NUM_CONS).write()
            }
        }
    }
})

window.addEventListener('offline', () => {
    console.warn("Se perdio la conexión a Internet...")

    // Limpiar lista de archivos no subidos
    app.nuList = []

    // Limpiar procesos en espera
    clearTimeout(app.timeout)
})

window.addEventListener('online', () => {
    console.info("Se recupero la conexión a Internet...")

    // Iniciar proceso
    app.process()
})