const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync') 

const adapter = new FileSync('nouploads.json')
const db = low(adapter)

db.defaults({
    nouploads: []
}).write()

module.exports = db
