const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync') 

const adapter = new FileSync('noexists.json')
const db = low(adapter)

db.defaults({
    noexists: []
}).write()

module.exports = db
